package com.example.playgroundgraddle.filter;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.example.playgroundgraddle.httpresponse.JWTResponse;
import com.example.playgroundgraddle.model.UserModel;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.core.env.Environment;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

public class JWTAuthFilter extends AbstractAuthenticationProcessingFilter {
    private AuthenticationManager authenticationManager;

    private Environment env;

    public JWTAuthFilter(String loginUrl, AuthenticationManager authenticationManager, Environment env) {
        super(loginUrl);

        this.authenticationManager = authenticationManager;
        this.env = env;
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest req,
                                                HttpServletResponse res) throws AuthenticationException {
        try {
            UserModel user = new ObjectMapper()
                    .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                    .readValue(req.getInputStream(), UserModel.class);

            UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(
                user.getEmail(),
                user.getPassword(),
                new ArrayList<>()
            );

            return authenticationManager.authenticate(token);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest req,
                                            HttpServletResponse res,
                                            FilterChain chain,
                                            Authentication auth) throws IOException {

        Integer tokenDuration = Integer.parseInt(this.env.getProperty("tokenDuration"));
        Integer refreshTokenDuration = Integer.parseInt(this.env.getProperty("refreshTokenDuration"));

        String oauthClientSecret = this.env.getProperty("oauthClientSecret");
        String token = JWT.create()
                .withSubject(((User) auth.getPrincipal()).getUsername())
                .withExpiresAt(new Date(System.currentTimeMillis() + tokenDuration))
                .sign(Algorithm.HMAC256(oauthClientSecret.getBytes()));

        String refreshToken = JWT.create()
                .withSubject(((User) auth.getPrincipal()).getUsername())
                .withExpiresAt(new Date(System.currentTimeMillis() + refreshTokenDuration))
                .sign(Algorithm.HMAC256(oauthClientSecret.getBytes()));

        ObjectMapper mapper = new ObjectMapper();
        JWTResponse jwtResponse = new JWTResponse(token, refreshToken, tokenDuration / 1000);
        res.setHeader("Content-Type", "application/json");
        res.getWriter().write(mapper.writeValueAsString(jwtResponse));
        res.getWriter().flush();
        res.getWriter().close();
    }
}
