package com.example.playgroundgraddle.httpresponse;

import com.example.playgroundgraddle.model.UserModel;

import java.util.List;

public class UserListResponse {
    private long filtered;

    private List<UserModel> data;

    public UserListResponse(long filtered, List<UserModel> data) {
        this.filtered = filtered;
        this.data = data;
    }

    public long getFiltered() {
        return filtered;
    }

    public void setFiltered(long filtered) {
        this.filtered = filtered;
    }

    public List<UserModel> getData() {
        return data;
    }

    public void setData(List<UserModel> data) {
        this.data = data;
    }
}
