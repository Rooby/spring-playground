package com.example.playgroundgraddle.repository;

import com.example.playgroundgraddle.model.UserModel;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface UserRepository extends PagingAndSortingRepository<UserModel, Integer> {
    UserModel findUserByEmail(String email);
}
