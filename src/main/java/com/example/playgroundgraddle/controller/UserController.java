package com.example.playgroundgraddle.controller;

import com.example.playgroundgraddle.httprequest.CheckEmailRequest;
import com.example.playgroundgraddle.httprequest.UserListRequest;
import com.example.playgroundgraddle.httpresponse.CheckEmailResponse;
import com.example.playgroundgraddle.httpresponse.UserListResponse;
import com.example.playgroundgraddle.model.UserModel;
import com.example.playgroundgraddle.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping(path="/api/user")
public class UserController {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @PostMapping(path="/register", consumes = "application/json", produces = "application/json")
    public @ResponseBody UserModel register (@RequestBody UserModel user)
    {
        String encodedPassword = passwordEncoder.encode(user.getPassword());
        user.setPassword(encodedPassword);
        userRepository.save(user);
        return user;
    }

    @PostMapping(path="/list")
    public @ResponseBody UserListResponse getAllUsers(@RequestBody UserListRequest request) {
        int pageNumber = request.getOffset() / request.getLimit();
        Sort sort = null;

        if (request.getSortDirection().equals("desc")) {
            sort = Sort.by(request.getSortColumn()).descending();
        } else {
            sort = Sort.by(request.getSortColumn()).ascending();
        }

        Pageable page = PageRequest.of(pageNumber, request.getLimit(), sort);
        Page<UserModel> users = userRepository.findAll(page);
        UserListResponse response = new UserListResponse(users.getTotalElements(), users.getContent());
        return response;
    }

    @PostMapping(path="/check-email", consumes = "application/json", produces = "application/json")
    public @ResponseBody CheckEmailResponse checkEmail(@RequestBody CheckEmailRequest request) {
        UserModel user = userRepository.findUserByEmail(request.getEmail());
        if (user != null) {
            return new CheckEmailResponse(true);
        } else {
            return new CheckEmailResponse(false);
        }
    }
}
